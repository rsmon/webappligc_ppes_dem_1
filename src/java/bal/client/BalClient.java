
package bal.client;
import entites.Client;

public interface BalClient {
    
    Float                 caAnnuel(Client pClient, int pAnnee);
    Float                 caAnnuel(Client pClient, int pAnnee, int pMois);
    Float                 caAnneeEnCours(Client pClient); 
    Float                 caMoisEnCours(Client pClient);
    
    Float                 resteARegler(Client pClient);
 
}
